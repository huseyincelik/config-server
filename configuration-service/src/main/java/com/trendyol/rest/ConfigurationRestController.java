package com.trendyol.rest;

import com.trendyol.assembler.ConfigurationResourceAssembler;
import com.trendyol.dto.request.ConfigurationRequestDto;
import com.trendyol.dto.response.ConfigurationResponseDto;
import com.trendyol.model.Configuration;
import com.trendyol.service.ConfigurationCreateService;
import com.trendyol.service.ConfigurationDeleteService;
import com.trendyol.service.ConfigurationReadService;
import com.trendyol.service.ConfigurationUpdateService;
import com.trendyol.validator.ConfigurationValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@ExposesResourceFor(ConfigurationResponseDto.class)
@RequestMapping("/api/configurations")
@Api(value = "/api/configurations", description = "Configuration crud operations")
public class ConfigurationRestController {

    private final ConfigurationCreateService configurationCreateService;
    private final ConfigurationReadService configurationReadService;
    private final ConfigurationUpdateService configurationUpdateService;
    private final ConfigurationDeleteService configurationDeleteService;
    private final ConfigurationResourceAssembler assembler;
    private final ConfigurationValidator configurationValidator;
    private final ModelMapper modelMapper;

    public ConfigurationRestController(ConfigurationCreateService configurationCreateService,
                                       ConfigurationUpdateService configurationUpdateService,
                                       ConfigurationReadService configurationReadService,
                                       ConfigurationDeleteService configurationDeleteService,
                                       ConfigurationResourceAssembler assembler,
                                       ConfigurationValidator configurationValidator, ModelMapper modelMapper) {
        this.configurationCreateService = configurationCreateService;
        this.configurationUpdateService = configurationUpdateService;
        this.configurationReadService = configurationReadService;
        this.assembler = assembler;
        this.configurationValidator = configurationValidator;
        this.modelMapper = modelMapper;
        this.configurationDeleteService = configurationDeleteService;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "For creating configuration", nickname = "save new configuration",
            notes = "You can crate an configuration using this method", response = ConfigurationResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "created successfully", response = ConfigurationResponseDto.class),
            @ApiResponse(code = 500, message = "Internal server error")}
    )
    public ResponseEntity<ConfigurationResponseDto> create(
            @Valid @RequestBody ConfigurationRequestDto configurationRequest) {
        configurationValidator.validate(configurationRequest);
        Configuration configuration = modelMapper.map(configurationRequest, Configuration.class);
        Configuration savedConfiguration = configurationCreateService.create(configuration);
        return new ResponseEntity<>(assembler.toResource(savedConfiguration), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<ConfigurationResponseDto> findById(@PathVariable("id") Long id) {
        Configuration configuration = configurationReadService.findById(id);
        return new ResponseEntity<>(assembler.toResource(configuration), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ApiOperation(value = "For updating configuration", nickname = "update existing configuration",
            notes = "You can update an configuration using this method", response = ConfigurationResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "updated successfully", response = ConfigurationResponseDto.class),
            @ApiResponse(code = 500, message = "Internal server error")}
    )
    public ResponseEntity<ConfigurationResponseDto> update(
            @PathVariable("id") Long id,
            @RequestBody @Valid ConfigurationRequestDto configurationRequest) {
        configurationValidator.validate(configurationRequest, id);
        Configuration configuration = configurationReadService.findById(id);
        modelMapper.map(configurationRequest, configuration);
        Configuration savedConfiguration = configurationUpdateService.update(configuration);

        return new ResponseEntity<>(assembler.toResource(savedConfiguration), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "to delete one configuration")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Deleted successfully"),
            @ApiResponse(code = 404, message = "configuration not found")}
    )
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        configurationDeleteService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(method = RequestMethod.GET, value = "search", produces = "application/x-spring-data-verbose+json")
    @ResponseBody
    public PagedResources<ConfigurationResponseDto> search(
            @RequestParam(value = "name") String name,
            @PageableDefault Pageable pageable, PagedResourcesAssembler<Configuration> pagedAssembler) {
        Page<Configuration> configurations = configurationReadService.search(name, pageable);

        return pagedAssembler.toResource(configurations, assembler);
    }

}
