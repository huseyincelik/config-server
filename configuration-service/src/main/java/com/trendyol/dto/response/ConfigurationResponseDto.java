package com.trendyol.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

import java.io.Serializable;

@Data
public class ConfigurationResponseDto extends ResourceSupport implements Serializable {

    private static final long serialVersionUID = 1470978973658046398L;

    @JsonProperty("id")
    private Long entityId;

    private Boolean isActive = Boolean.TRUE;

    private String name;

    private String type;

    private String value;

    private String applicationName;
}
