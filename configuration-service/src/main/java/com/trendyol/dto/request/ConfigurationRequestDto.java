package com.trendyol.dto.request;

import com.trendyol.model.ConfigurationType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class ConfigurationRequestDto implements Serializable {

    private static final long serialVersionUID = -7571114402307823524L;

    @ApiModelProperty(value = "isActive")
    @NotNull
    private Boolean isActive = Boolean.TRUE;

    @ApiModelProperty(value = "name")
    @NotNull
    private String name;

    @ApiModelProperty(value = "type")
    @NotNull
    private ConfigurationType type;

    @ApiModelProperty(value = "value")
    @NotNull
    private String value;

    @ApiModelProperty(value = "applicationName")
    @NotNull
    private String applicationName;
}
