package com.trendyol.assembler;


import com.trendyol.dto.response.ConfigurationResponseDto;
import com.trendyol.model.Configuration;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class ConfigurationResourceAssembler
        extends ResourceAssemblerSupport<Configuration, ConfigurationResponseDto> {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private EntityLinks entityLinks;

    public ConfigurationResourceAssembler() {
        super(Configuration.class, ConfigurationResponseDto.class);
    }

    @Override
    public ConfigurationResponseDto toResource(Configuration configuration) {
        ConfigurationResponseDto responseDto = modelMapper.map(configuration, ConfigurationResponseDto.class);
        responseDto.setEntityId(configuration.getId());
        responseDto.add(entityLinks.linkToSingleResource(ConfigurationResponseDto.class, responseDto.getEntityId()));
        return responseDto;
    }
}
