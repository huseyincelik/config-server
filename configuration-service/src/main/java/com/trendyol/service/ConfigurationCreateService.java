package com.trendyol.service;

import com.trendyol.model.Configuration;
import com.trendyol.repository.ConfigurationRepository;
import org.springframework.stereotype.Service;

@Service
public class ConfigurationCreateService {

    private final ConfigurationRepository configurationRepository;

    public ConfigurationCreateService(ConfigurationRepository configurationRepository) {
        this.configurationRepository = configurationRepository;
    }

    public Configuration create(Configuration configuration) {
        return configurationRepository.save(configuration);
    }

}
