package com.trendyol.service;

import com.trendyol.exception.NotFoundException;
import com.trendyol.repository.ConfigurationRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class ConfigurationDeleteService {

    private final ConfigurationRepository configurationRepository;

    public ConfigurationDeleteService(ConfigurationRepository configurationRepository) {
        this.configurationRepository = configurationRepository;
    }

    public void delete(Long id) {
        try {
            configurationRepository.delete(id);
        } catch (EmptyResultDataAccessException exception) {
            throw new NotFoundException();
        }
    }

}
