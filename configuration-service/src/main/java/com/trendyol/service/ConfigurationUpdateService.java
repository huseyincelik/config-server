package com.trendyol.service;

import com.trendyol.model.Configuration;
import com.trendyol.repository.ConfigurationRepository;
import org.springframework.stereotype.Service;

@Service
public class ConfigurationUpdateService {

    private final ConfigurationRepository configurationRepository;

    public ConfigurationUpdateService(ConfigurationRepository configurationRepository) {
        this.configurationRepository = configurationRepository;
    }

    public Configuration update(Configuration configuration) {
        return configurationRepository.save(configuration);
    }

}
