package com.trendyol.service;

import com.trendyol.exception.NotFoundException;
import com.trendyol.model.Configuration;
import com.trendyol.repository.ConfigurationRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ConfigurationReadService {

    private final ConfigurationRepository configurationRepository;

    public ConfigurationReadService(ConfigurationRepository configurationRepository) {
        this.configurationRepository = configurationRepository;
    }

    public Configuration findById(Long id) {
        return Optional
                .ofNullable(configurationRepository.findOne(id))
                .orElseThrow(NotFoundException::new);
    }

    public Page<Configuration> search(String name, Pageable pageable) {
        return configurationRepository.findByNameContainingIgnoreCase(name, pageable);
    }

}
