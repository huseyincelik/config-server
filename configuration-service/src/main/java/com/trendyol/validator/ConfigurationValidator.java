package com.trendyol.validator;

import com.trendyol.dto.request.ConfigurationRequestDto;
import com.trendyol.exception.ConflictException;
import com.trendyol.exception.TypeMismatchException;
import com.trendyol.model.Configuration;
import com.trendyol.repository.ConfigurationRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ConfigurationValidator {

    private final ConfigurationRepository configurationRepository;

    public ConfigurationValidator(ConfigurationRepository configurationRepository) {
        this.configurationRepository = configurationRepository;
    }

    public void validate(ConfigurationRequestDto configurationRequestDto) {
        validate(configurationRequestDto, null);
    }

    public void validate(ConfigurationRequestDto configurationRequestDto, Long id) {
        validateType(configurationRequestDto);
        validateUniqueName(configurationRequestDto, id);
    }

    private void validateUniqueName(ConfigurationRequestDto configurationRequestDto, Long id) {
        List<Configuration> configurations = configurationRepository.findByNameAndApplicationNameAndIsActive(
                configurationRequestDto.getName(),
                configurationRequestDto.getApplicationName(),
                true);
        if (configurations.isEmpty()) {
            return;
        }
        if (!configurations.get(0).getId().equals(id)) {
            throw new ConflictException();
        }
    }

    private void validateType(ConfigurationRequestDto configurationRequestDto) {
        if (!configurationRequestDto.getType().validValue(configurationRequestDto.getValue())) {
            throw new TypeMismatchException();
        }
    }

}
