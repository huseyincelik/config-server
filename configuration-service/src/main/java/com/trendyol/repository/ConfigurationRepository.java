package com.trendyol.repository;

import com.trendyol.model.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConfigurationRepository extends CrudRepository<Configuration, Long> {

    Page<Configuration> findByNameContainingIgnoreCase(String name, Pageable pageable);

    List<Configuration> findByNameAndApplicationNameAndIsActive(String name, String applicationName, Boolean isActive);

}
