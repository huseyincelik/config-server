package com.trendyol.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Data
@Entity(name = "CONFIGURATION")
@SequenceGenerator(name = "SEQ_CONFIGURATION", sequenceName = "SEQ_CONFIGURATION")
public class Configuration {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive = Boolean.TRUE;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TYPE")
    @Enumerated(EnumType.STRING)
    private ConfigurationType type;

    @Column(name = "VALUE")
    private String value;

    @Column(name = "APPLICATION_NAME")
    private String applicationName;

}
