package com.trendyol.model;

public enum ConfigurationType {

    STRING {
        @Override
        public boolean validValue(String value) {
            return true;
        }
    }, BOOLEAN {
        @Override
        public boolean validValue(String value) {
            try {
                Boolean.valueOf(value);
                return true;
            } catch (Exception exc) {
                return false;
            }
        }
    }, INTEGER {
        @Override
        public boolean validValue(String value) {
            try {
                Integer.valueOf(value);
                return true;
            } catch (Exception exc) {
                return false;
            }
        }
    }, DOUBLE {
        @Override
        public boolean validValue(String value) {
            try {
                Double.valueOf(value);
                return true;
            } catch (Exception exc) {
                return false;
            }
        }
    };

    public abstract boolean validValue(String value);
}
