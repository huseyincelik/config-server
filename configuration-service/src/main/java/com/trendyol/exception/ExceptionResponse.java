package com.trendyol.exception;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ExceptionResponse {

    private String errorCode;
    private String errorMessage;
    private List<String> errors = new ArrayList<>();

}
