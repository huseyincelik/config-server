package com.trendyol.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Type and value type do not match")
@Data
public class TypeMismatchException extends RuntimeException {

}
