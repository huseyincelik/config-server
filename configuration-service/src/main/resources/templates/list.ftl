<div class="generic-container">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">Configuration Form</span></div>
        <div class="panel-body">
            <div class="formcontainer">
                <div class="alert alert-success" role="alert" ng-if="ctrl.successMessage">{{ctrl.successMessage}}</div>
                <div class="alert alert-danger" role="alert" ng-if="ctrl.errorMessage">{{ctrl.errorMessage}}</div>
                <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                    <input type="hidden" ng-model="ctrl.configuration.id"/>

                    <div class="row" ng-if="ctrl.configuration.id">
                        <div class="form-group col-md-12">
                            <label class="col-md-2 control-label" for="configId">Id</label>
                            <div class="col-md-7">
                                <input type="text" ng-model="ctrl.configuration.id" id="configId" disabled
                                       class="form-control input-sm"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="col-md-2 control-label" for="isActive">Is Active</label>
                            <div class="col-md-7">
                                <input type="checkbox" ng-model="ctrl.configuration.isActive" id="isActive"
                                       class="form-control input-sm"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="col-md-2 control-label" for="name">Name</label>
                            <div class="col-md-7">
                                <input type="text" ng-model="ctrl.configuration.name" id="name"
                                       class="form-control input-sm" required/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="col-md-2 control-label" for="name">Type</label>
                            <div class="col-md-7">
                                <select name="singleSelect" id="singleSelect" ng-model="ctrl.configuration.type"
                                        ng-change="ctrl.typeChanged()"
                                        required>
                                    <option value="">Please Select</option>
                                    <option value="STRING">String</option>
                                    <option value="BOOLEAN">Boolean</option>
                                    <option value="INTEGER">Integer</option>
                                    <option value="DOUBLE">Double</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="col-md-2 control-label" for="applicationName">Application Name</label>
                            <div class="col-md-7">
                                <input type="text" ng-model="ctrl.configuration.applicationName" id="applicationName"
                                       class="form-control input-sm" required/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="col-md-2 control-label">Value</label>
                            <div class="col-md-7">
                                <input type="text" ng-model="ctrl.configuration.value" id="configValueString"
                                       class="form-control input-sm" required
                                       ng-if="ctrl.configuration.type == 'STRING'"/>

                                <input type="text" ng-model="ctrl.configuration.value" id="configValueInteger"
                                       class="form-control input-sm" required
                                       ng-if="ctrl.configuration.type == 'INTEGER'"
                                       ng-pattern="ctrl.onlyIntegers"/>

                                <input type="text" ng-model="ctrl.configuration.value" id="configValueDouble"
                                       class="form-control input-sm" required
                                       ng-if="ctrl.configuration.type == 'DOUBLE'"
                                       ng-pattern="ctrl.onlyNumbers"/>

                                <input type="checkbox" ng-model="ctrl.configuration.value" id="configValueBoolean"
                                       class="form-control input-sm"
                                       ng-if="ctrl.configuration.type == 'BOOLEAN'"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-actions floatRight">
                            <input type="submit" value="{{!ctrl.configuration.id ? 'Add' : 'Update'}}"
                                   class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid || myForm.$pristine">
                            <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm"
                                    ng-disabled="myForm.$pristine">Reset Form
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-body">
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-2 control-label" for="name">Name</label>
                    <div class="col-md-7">
                        <input type="text" ng-model="ctrl.search.name" id="searchName"
                               class="form-control input-sm"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-actions floatRight">
                    <button type="button" ng-click="ctrl.searchConfigurations()" class="btn btn-warning btn-sm" >Search
                    </button>
                </div>
            </div>
        </div>
    </div>


    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">List of Configurations </span></div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Is Active</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Value</th>
                        <th>Application Name</th>
                        <th width="100"></th>
                        <th width="100"></th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr ng-repeat="each in ctrl.getAllConfigurations().content">
                        <td>{{each.id}}</td>
                        <td>{{each.isActive}}</td>
                        <td>{{each.name}}</td>
                        <td>{{each.type}}</td>
                        <td>{{each.value}}</td>
                        <td>{{each.applicationName}}</td>
                        <td>
                            <button type="button" ng-click="ctrl.editConfiguration(each.id)"
                                    class="btn btn-success custom-width">
                                Edit
                            </button>
                        </td>
                        <td>
                            <button type="button" ng-click="ctrl.removeConfiguration(each.id)"
                                    class="btn btn-danger custom-width">
                                Remove
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>