var app = angular.module('configApp', ['ui.router', 'ngStorage']);

app.constant('urls', {
    BASE: 'http://localhost:8080',
    CONFIGURATION_SERVICE_API: 'http://localhost:8080/api/configurations/'
});

app.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'partials/list',
                controller: 'ConfigurationController',
                controllerAs: 'ctrl',
                resolve: {
                    configurations: function ($q, ConfigurationService) {
                        console.log('Load all configurations');
                        var deferred = $q.defer();
                        ConfigurationService.loadAllConfigurations().then(deferred.resolve, deferred.resolve);
                        return deferred.promise;
                    }
                }
            });
        $urlRouterProvider.otherwise('/');
    }]
);