'use strict';

angular.module('configApp').controller('ConfigurationController',
    ['ConfigurationService', '$scope', function (ConfigurationService, $scope) {

        var self = this;
        self.configuration = {isActive: true};
        self.configurations = [];
        self.search = {name: ""};

        self.submit = submit;
        self.getAllConfigurations = getAllConfigurations;
        self.searchConfigurations = searchConfigurations;
        self.createConfiguration = createConfiguration;
        self.updateConfiguration = updateConfiguration;
        self.removeConfiguration = removeConfiguration;
        self.editConfiguration = editConfiguration;
        self.typeChanged = typeChanged;
        self.reset = reset;

        self.successMessage = '';
        self.errorMessage = '';
        self.done = false;

        self.onlyIntegers = /^\d+$/;
        self.onlyNumbers = /^\d+([,.]\d+)?$/;

        function submit() {
            if (self.configuration.id === undefined || self.configuration.id === null) {
                createConfiguration(self.configuration);
            } else {
                updateConfiguration(self.configuration, self.configuration.id);
            }
        }

        function createConfiguration(configuration) {
            ConfigurationService.createConfiguration(configuration)
                .then(
                    function (response) {
                        self.successMessage = 'Configuration created successfully';
                        self.errorMessage = '';
                        self.done = true;
                        self.configuration = {};
                        $scope.myForm.$setPristine();
                    },
                    function (errResponse) {
                        self.errorMessage = 'Error while creating Configuration: ' + errResponse.data.errorMessage;
                        self.successMessage = '';
                    }
                );
        }


        function updateConfiguration(configuration, id) {
            ConfigurationService.updateConfiguration(configuration, id)
                .then(
                    function (response) {
                        self.successMessage = 'Configuration updated successfully';
                        self.errorMessage = '';
                        self.done = true;
                        $scope.myForm.$setPristine();
                    },
                    function (errResponse) {
                        self.errorMessage = 'Error while updating Configuration ' + errResponse.data;
                        self.successMessage = '';
                    }
                );
        }

        function searchConfigurations() {
            ConfigurationService.loadAllConfigurations(self.search.name);
        }

        function removeConfiguration(id) {
            ConfigurationService.removeConfiguration(id);
        }


        function getAllConfigurations() {
            return ConfigurationService.getAllConfigurations();
        }

        function editConfiguration(id) {
            self.successMessage = '';
            self.errorMessage = '';
            ConfigurationService.getConfiguration(id).then(
                function (configuration) {
                    self.configuration = configuration;
                }
            );
        }

        function reset() {
            self.successMessage = '';
            self.errorMessage = '';
            resetConfiguration();
            $scope.myForm.$setPristine(); //reset Form
        }

        function resetConfiguration() {
            self.configuration = {isAtive: true};
        }

        function typeChanged() {
            self.configuration.value = "";
        }
    }
    ]
);