'use strict';

angular.module('configApp').factory('ConfigurationService',
    ['$localStorage', '$http', '$q', 'urls',
        function ($localStorage, $http, $q, urls) {

            var factory = {
                loadAllConfigurations: loadAllConfigurations,
                getAllConfigurations: getAllConfigurations,
                getConfiguration: getConfiguration,
                createConfiguration: createConfiguration,
                updateConfiguration: updateConfiguration,
                removeConfiguration: removeConfiguration,
            };

            return factory;

            function loadAllConfigurations(name) {
                var deferred = $q.defer();
                if (!name) {
                    name = ""
                }
                $http.get(urls.CONFIGURATION_SERVICE_API + "search?size=1000&name="+name)
                    .then(
                        function (response) {
                            $localStorage.configurations = response.data;
                            deferred.resolve(response);
                        },
                        function (errResponse) {
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function getAllConfigurations() {
                return $localStorage.configurations;
            }

            function getConfiguration(id) {
                var deferred = $q.defer();
                $http.get(urls.CONFIGURATION_SERVICE_API + id)
                    .then(
                        function (response) {
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function createConfiguration(configuration) {
                var deferred = $q.defer();
                $http.post(urls.CONFIGURATION_SERVICE_API, configuration)
                    .then(
                        function (response) {
                            loadAllConfigurations();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function updateConfiguration(configuration, id) {
                var deferred = $q.defer();
                $http.put(urls.CONFIGURATION_SERVICE_API + id, configuration)
                    .then(
                        function (response) {
                            loadAllConfigurations();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function removeConfiguration(id) {
                var deferred = $q.defer();
                $http.delete(urls.CONFIGURATION_SERVICE_API + id)
                    .then(
                        function (response) {
                            loadAllConfigurations();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

        }
    ]);