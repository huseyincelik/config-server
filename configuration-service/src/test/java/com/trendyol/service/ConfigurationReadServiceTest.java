package com.trendyol.service;

import com.trendyol.exception.NotFoundException;
import com.trendyol.repository.ConfigurationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ConfigurationReadServiceTest {

    @InjectMocks
    private ConfigurationReadService configurationReadService;

    @Mock
    private ConfigurationRepository configurationRepository;

    @Test(expected = NotFoundException.class)
    public void shouldThrowExceptionIfConfigurationIsNotFound() throws Exception {
        configurationReadService.findById(13L);
    }
}