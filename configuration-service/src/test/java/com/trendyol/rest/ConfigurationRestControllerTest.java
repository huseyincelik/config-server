package com.trendyol.rest;

import com.trendyol.assembler.ConfigurationResourceAssembler;
import com.trendyol.dto.request.ConfigurationRequestDto;
import com.trendyol.dto.response.ConfigurationResponseDto;
import com.trendyol.model.Configuration;
import com.trendyol.service.ConfigurationCreateService;
import com.trendyol.service.ConfigurationDeleteService;
import com.trendyol.service.ConfigurationReadService;
import com.trendyol.service.ConfigurationUpdateService;
import com.trendyol.validator.ConfigurationValidator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfigurationRestControllerTest {

    @InjectMocks
    private ConfigurationRestController configurationRestController;

    @Mock
    private ConfigurationCreateService configurationCreateService;

    @Mock
    private ConfigurationReadService configurationReadService;

    @Mock
    private ConfigurationUpdateService configurationUpdateService;

    @Mock
    private ConfigurationDeleteService configurationDeleteService;

    @Mock
    private ConfigurationResourceAssembler assembler;

    @Mock
    private ConfigurationValidator configurationValidator;

    @Mock
    private ModelMapper modelMapper;

    @Test
    public void shouldReturnFoundConfiguration() throws Exception {
        Configuration configuration = new Configuration();
        configuration.setId(15L);

        ConfigurationResponseDto configurationResponse = new ConfigurationResponseDto();
        configurationResponse.setEntityId(15L);

        when(configurationReadService.findById(15L)).thenReturn(configuration);
        when(assembler.toResource(configuration)).thenReturn(configurationResponse);

        ResponseEntity<ConfigurationResponseDto> foundResource = configurationRestController.findById(15L);

        assertEquals(foundResource.getBody().getEntityId(), configuration.getId());
    }

    @Test
    public void shouldCreateConfiguration() throws Exception {
        ConfigurationRequestDto configurationRequest = new ConfigurationRequestDto();
        configurationRequest.setName("name1");

        Configuration configuration = new Configuration();

        when(modelMapper.map(configurationRequest, Configuration.class)).thenReturn(configuration);

        configurationRestController.create(configurationRequest);

        InOrder inOrder = inOrder(configurationValidator, configurationCreateService);
        inOrder.verify(configurationValidator).validate(configurationRequest);
        inOrder.verify(configurationCreateService).create(configuration);
    }

    @Test
    public void shouldUpdateConfiguration() throws Exception {
        ConfigurationRequestDto configurationRequest = new ConfigurationRequestDto();

        Configuration foundConfiguration = new Configuration();

        when(configurationReadService.findById(13L)).thenReturn(foundConfiguration);

        configurationRestController.update(13L, configurationRequest);

        InOrder inOrder = inOrder(modelMapper, configurationValidator, configurationUpdateService);
        inOrder.verify(configurationValidator).validate(configurationRequest, 13L);
        inOrder.verify(modelMapper).map(configurationRequest, foundConfiguration);
        inOrder.verify(configurationUpdateService).update(foundConfiguration);
    }

    @Test
    public void shouldDelete() throws Exception {
        configurationRestController.delete(17L);

        verify(configurationDeleteService).delete(17L);
    }

    @Test
    public void shouldSearch() throws Exception {
        configurationRestController.search("configName", new PageRequest(0, 10), mock(PagedResourcesAssembler.class));

        verify(configurationReadService).search("configName", new PageRequest(0, 10));
    }
}