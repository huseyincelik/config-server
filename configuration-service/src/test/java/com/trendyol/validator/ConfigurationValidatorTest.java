package com.trendyol.validator;

import com.trendyol.dto.request.ConfigurationRequestDto;
import com.trendyol.exception.ConflictException;
import com.trendyol.exception.TypeMismatchException;
import com.trendyol.model.Configuration;
import com.trendyol.model.ConfigurationType;
import com.trendyol.repository.ConfigurationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfigurationValidatorTest {

    @InjectMocks
    private ConfigurationValidator configurationValidator;

    @Mock
    private ConfigurationRepository configurationRepository;

    @Test(expected = TypeMismatchException.class)
    public void shouldInvalidateForTypeMatch() throws Exception {
        ConfigurationRequestDto configurationRequestDto = new ConfigurationRequestDto();
        configurationRequestDto.setValue("asd");
        configurationRequestDto.setType(ConfigurationType.INTEGER);

        configurationValidator.validate(configurationRequestDto, null);
    }

    @Test(expected = ConflictException.class)
    public void shouldInvalidateForNameConflictsAndNewRecord() throws Exception {
        ConfigurationRequestDto configurationRequestDto = new ConfigurationRequestDto();
        configurationRequestDto.setValue("3");
        configurationRequestDto.setName("duplicated name");
        configurationRequestDto.setApplicationName("app name");
        configurationRequestDto.setType(ConfigurationType.INTEGER);

        Configuration configuration = new Configuration();
        configuration.setId(15L);

        when(configurationRepository
                .findByNameAndApplicationNameAndIsActive("duplicated name", "app name", Boolean.TRUE))
                .thenReturn(Arrays.asList(configuration));

        configurationValidator.validate(configurationRequestDto, null);
    }

    @Test(expected = ConflictException.class)
    public void shouldInvalidateForNameConflictsAndOldRecords() throws Exception {
        ConfigurationRequestDto configurationRequestDto = new ConfigurationRequestDto();
        configurationRequestDto.setValue("3");
        configurationRequestDto.setName("duplicated name");
        configurationRequestDto.setApplicationName("app name");
        configurationRequestDto.setType(ConfigurationType.INTEGER);

        Configuration configuration = new Configuration();
        configuration.setId(15L);

        when(configurationRepository
                .findByNameAndApplicationNameAndIsActive("duplicated name", "app name", Boolean.TRUE))
                .thenReturn(Arrays.asList(configuration));

        configurationValidator.validate(configurationRequestDto, 13L);
    }

    @Test
    public void shouldNotInvalidateForNameConflictsAndOldRecordsIfIdsAreMatching() throws Exception {
        ConfigurationRequestDto configurationRequestDto = new ConfigurationRequestDto();
        configurationRequestDto.setValue("3");
        configurationRequestDto.setName("duplicated name");
        configurationRequestDto.setApplicationName("app name");
        configurationRequestDto.setType(ConfigurationType.INTEGER);

        Configuration configuration = new Configuration();
        configuration.setId(15L);

        when(configurationRepository
                .findByNameAndApplicationNameAndIsActive("duplicated name", "app name", Boolean.TRUE))
                .thenReturn(Arrays.asList(configuration));

        configurationValidator.validate(configurationRequestDto, 15L);
    }

    @Test
    public void shouldNotInvalidateForNoMatchingRecords() throws Exception {
        ConfigurationRequestDto configurationRequestDto = new ConfigurationRequestDto();
        configurationRequestDto.setValue("3");
        configurationRequestDto.setName("duplicated name");
        configurationRequestDto.setApplicationName("app name");
        configurationRequestDto.setType(ConfigurationType.INTEGER);

        Configuration configuration = new Configuration();
        configuration.setId(15L);

        configurationValidator.validate(configurationRequestDto, 15L);
        configurationValidator.validate(configurationRequestDto);
    }
}