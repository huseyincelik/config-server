# My project's README


# Configuration Project

Includes both reader and web interface projects.

## Configuration Reader

After mvn package, configuration-reader-1.0-SNAPSHOT-jar-with-dependencies.jar can be included as external library.

## Configuration Service

You can use either docker-compose up or mvn spring-boot:run with a localhost:3306 mysql running. Docker can take some time because of mysql initialization.
