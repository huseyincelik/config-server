package com.trendyol.enums;


/**
 * Configuration types and convertion operations.
 */
public enum ConfigurationType {

    /**
     * String type for configurations.
     */
    STRING {
        @Override
        public Object convertValue(final String configValue) {
            return configValue;
        }
    },

    /**
     * Boolean type for configurations.
     */
    BOOLEAN {
        @Override
        public Object convertValue(final String configValue) {
            return Boolean.valueOf(configValue);
        }
    },

    /**
     * Integer type for configurations.
     */
    INTEGER {
        @Override
        public Object convertValue(final String configValue) {
            return Integer.valueOf(configValue);
        }
    },

    /**
     * Double type for configurations.
     */
    DOUBLE {
        @Override
        public Object convertValue(final String configValue) {
            return Double.valueOf(configValue);
        }
    };


    /**
     * @param configValue configuration value as string to convert
     * @return converted to corresponding type
     * @since 3.5
     */
    public abstract Object convertValue(final String configValue);

}
