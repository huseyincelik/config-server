package com.trendyol;


import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ConfigurationReader {

    private ConfigurationRepository configurationRepository;

    private Map<String, Object> configurations = new HashMap<>();

    /**
     * @param applicationName  determines which configuration values should be retrieved.
     * @param connectionString storage connection info.
     * @param interval         checking for new values from storage, must be millisecond.
     */
    public ConfigurationReader(String applicationName, String connectionString, long interval) {
        this(interval, new ConfigurationRepository(connectionString, applicationName));
    }

    /**
     * @param configurationRepository is just for testing purposes.
     */
    ConfigurationReader(long interval,
                        ConfigurationRepository configurationRepository) {
        this.configurationRepository = configurationRepository;
        TimerTask timerTask = new TimerTask() {
            public void run() {
                resetConfigurations();
            }
        };
        new Timer().scheduleAtFixedRate(timerTask, interval, interval);
        resetConfigurations();
    }

    private void resetConfigurations() {
        try {
            configurations = configurationRepository.findAll();
        } catch (Exception e) {
        }
    }

    /**
     * @param key the key for the configuration.
     * @param <T> the type of the confguration value.
     * @return the found configuration value with given type
     */
    public <T> T getValue(String key) {
        return (T) configurations.get(key);
    }

}
