package com.trendyol;

import com.trendyol.enums.ConfigurationType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

class ResultSetToConfigurationConverter {

    Map<String, Object> convert(ResultSet rs) throws SQLException {
        Map<String, Object> newConfigurations = new HashMap<>();

        while (rs.next()) {

            ConfigurationType configType = ConfigurationType.valueOf(rs.getString("type"));

            String configName = rs.getString("name");
            String configStringValue = rs.getString("value");

            newConfigurations.put(configName, configType.convertValue(configStringValue));
        }

        return newConfigurations;
    }

}
