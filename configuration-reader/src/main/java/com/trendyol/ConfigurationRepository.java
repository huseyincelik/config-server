package com.trendyol;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

public class ConfigurationRepository {

    private static final String USERNAME = "dev_user";
    private static final String PASSWORD = "123qwe";
    private static final String QUERY_STRING = "SELECT name,value,type FROM configuration where APPLICATION_NAME=?";

    private final String connectionString;
    private final String applicationName;
    private final ResultSetToConfigurationConverter converter;

    ConfigurationRepository(String connectionString, String applicationName) {
        this(new ResultSetToConfigurationConverter(), connectionString, applicationName);
    }

    ConfigurationRepository(
            ResultSetToConfigurationConverter converter,
            String connectionString,
            String applicationName) {
        this.converter = converter;
        this.connectionString = connectionString;
        this.applicationName = applicationName;
    }

    Map<String, Object> findAll() throws SQLException {
        try (Connection connection = DriverManager.getConnection(connectionString, USERNAME, PASSWORD)) {
            try (PreparedStatement stmt = connection.prepareStatement(QUERY_STRING)) {
                stmt.setString(1, applicationName);
                try (ResultSet rs = stmt.executeQuery()) {
                    return converter.convert(rs);
                }
            }
        }
    }

}
