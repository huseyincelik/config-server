package com.trendyol;

import org.junit.Test;

import java.sql.ResultSet;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ResultSetToConfigurationConverterTest {

    private ResultSetToConfigurationConverter converter = new ResultSetToConfigurationConverter();

    @Test
    public void shouldConvertResultSetToConfigurationMap() throws Exception {
        ResultSet resultSet = mock(ResultSet.class);

        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet.getString("name")).thenReturn("config name1").thenReturn("config name2");
        when(resultSet.getString("value")).thenReturn("config value1").thenReturn("3");
        when(resultSet.getString("type")).thenReturn("STRING").thenReturn("INTEGER");

        Map<String, Object> configurationMap = converter.convert(resultSet);

        assertEquals(configurationMap.get("config name1"), "config value1");
        assertEquals(configurationMap.get("config name2"), 3);
    }
}