package com.trendyol;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfigurationReaderTest {

    private ConfigurationReader configurationReader;

    @Mock
    private ConfigurationRepository configurationRepository;

    @Test
    public void shouldReturnConfigurationValue() throws Exception {
        Map<String, Object> configurationMap = new HashMap<>();
        configurationMap.put("key1", 1.3D);
        configurationMap.put("key2", "str value");
        configurationMap.put("key3", Boolean.TRUE);
        configurationMap.put("key4", 3);

        when(configurationRepository.findAll()).thenReturn(configurationMap);

        configurationReader = new ConfigurationReader(10000L, configurationRepository);

        assertEquals(configurationReader.<Double>getValue("key1"), Double.valueOf(1.3D));
        assertEquals(configurationReader.<String>getValue("key2"), "str value");
        assertEquals(configurationReader.<Boolean>getValue("key3"), Boolean.TRUE);
        assertEquals(configurationReader.<Integer>getValue("key4"), Integer.valueOf(3));
    }

    @Test
    public void shouldKeepWorkingWithExistingConfigurations() throws Exception {
        Map<String, Object> configurationMap = new HashMap<>();
        configurationMap.put("key1", 1.3D);
        configurationMap.put("key2", "str value");
        configurationMap.put("key3", Boolean.TRUE);
        configurationMap.put("key4", 3);

        when(configurationRepository.findAll()).thenReturn(configurationMap);

        configurationReader = new ConfigurationReader(1000L, configurationRepository);

        when(configurationRepository.findAll()).thenThrow(new SQLException());

        Thread.sleep(3000L);

        assertEquals(configurationReader.<Double>getValue("key1"), Double.valueOf(1.3D));
        assertEquals(configurationReader.<String>getValue("key2"), "str value");
        assertEquals(configurationReader.<Boolean>getValue("key3"), Boolean.TRUE);
        assertEquals(configurationReader.<Integer>getValue("key4"), Integer.valueOf(3));
    }
}